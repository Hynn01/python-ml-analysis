#!/bin/sh
# Get current user's user ID and group ID
user_id=$(id -u)
group_id=$(id -g)

# When the current user is root, use a normal user's IDs, as `groupadd` and `useradd` don't allow specifying `-g 0`
[[ "$user_id" == "0" ]] && user_id="1000"
[[ "$group_id" == "0" ]] && group_id="1000"

docker build -t python-ml-analyser \
  --build-arg user_id=$(id -u) \
  --build-arg group_id=$(id -g) \
  .

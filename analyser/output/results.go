package output

import (
	"fmt"
	"io/ioutil"

	"github.com/fatih/color"
	"gopkg.in/yaml.v3"

	"gitlab.com/bvobart/python-ml-analysis/input"
	"gitlab.com/bvobart/python-ml-analysis/pylint"
	"gitlab.com/bvobart/python-ml-analysis/utils"
)

type Results struct {
	ProjectName string `yaml:"name"`
	ProjectURL  string `yaml:"url"`
	// in case any error occurred during the analysis
	Error string `yaml:"error,omitempty"`

	// the cloned commit hash
	CommitHash string `yaml:"commitHash"`
	// the number of commits made to the default branch of the repository
	NumCommits int `yaml:"numCommits"`
	// the number of contributors to the repository
	NumContributors int `yaml:"numContributors"`

	// the number of pure Python (.py) files in the project
	NumPythonFiles int `yaml:"numPythonFiles"`
	// the number of Jupyter Notebook (.ipynb) files in the project.
	NumIPynbFiles int `yaml:"numIPynbFiles"`

	// the number of lines of pure Python code (code in .py files) in the project
	NumPythonLines int `yaml:"numPythonLines"`
	// the number of lines of Python in Jupyter Notebook files (.ipynb) in the project
	NumIPynbLines int `yaml:"numIPynbLines"`

	// whether the repository contains a valid requirements file (requirements.txt or setup.py).
	// If false, then one had to be generated or custom requirements.txt contents had to be specified.
	HasRequirementsFile bool `yaml:"hasRequirementsFile"`
	// if HasRequirementsFile is false, then either a requirements file was generated,
	// or custom requirements.txt contents were provided in the dataset. Either way, we print it here if that's the case.
	CustomRequirements string `yaml:"customRequirements,omitempty"`

	// whether the project defined any extraRequirements
	HasExtraRequirements bool `yaml:"hasExtraRequirements"`
	// if the project defined any extraRequirements, then these are printed here.
	ExtraRequirements []string `yaml:"extraRequirements,omitempty"`

	// How many Pylint messages per file path (e.g. folder/file1.py),
	// per message type (e.g. convention)
	// per message symbol (e.g. bad-indentation) were found
	// or per message symbol nested in message type
	MessagesPerPath map[string]int `yaml:"messagesPerFile"`

	// How many messages Pylint emitted per message type, per symbol
	NumMessages map[pylint.MessageType]map[string]int `yaml:"numMessages"`

	// the raw warnings and errors that are returned by `pylint`
	LintMessages []pylint.Message `yaml:"lintMessages"`
}

type ResultsFile struct {
	Results []Results `yaml:"results"`
}

// ------------------------------------------------------------------

func NewResults(project input.Project) Results {
	return Results{
		ProjectName:          project.Name,
		ProjectURL:           project.CloneURL,
		HasExtraRequirements: len(project.ExtraRequirements) > 0,
		ExtraRequirements:    project.ExtraRequirements,
	}
}

func FromYAMLFile(filename string) (*ResultsFile, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var resfile ResultsFile
	err = yaml.Unmarshal(data, &resfile)
	if err != nil {
		return nil, fmt.Errorf("failed to parse file %s: %w", filename, err)
	}

	return &resfile, nil
}

// ------------------------------------------------------------------

func (res Results) String() string {
	name := fmt.Sprintf(color.YellowString("  Name: ")+"%s", res.ProjectName)
	hash := fmt.Sprintf(color.YellowString("  CommitHash: ")+"%s", res.CommitHash)
	commits := fmt.Sprintf(color.YellowString("  Number of commits: ")+"%d", res.NumCommits)
	contributors := fmt.Sprintf(color.YellowString("  Number of contributors: ")+"%d", res.NumContributors)
	hasReqFile := fmt.Sprintf(color.YellowString("  Has requirements.txt: %s"), utils.ColouredBool(res.HasRequirementsFile).String())
	pythonCode := fmt.Sprintf(color.YellowString("  Pure Python files and total lines of code: ")+"%d - %d", res.NumPythonFiles, res.NumPythonLines)
	ipynbCode := fmt.Sprintf(color.YellowString("  Jupyter Notebook files and total lines of code: ")+"%d - %d", res.NumIPynbFiles, res.NumIPynbLines)
	totalMsgs := fmt.Sprintf(color.YellowString("  Total messages: ")+"%d", len(res.LintMessages))
	pathMsgs := fmt.Sprintf(color.YellowString("  Pylint messages per path: ")+"%+v", res.MessagesPerPath)
	numMsgs := fmt.Sprintf(color.YellowString("  Number of Pylint messages: ")+"%s", formatNumMessages(res.NumMessages))
	return fmt.Sprintf("\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n", name, hash, commits, contributors, hasReqFile, pythonCode, ipynbCode, totalMsgs, pathMsgs, numMsgs)
}

func formatNumMessages(msgs map[pylint.MessageType]map[string]int) string {
	formatted := "\n"
	for _, msgType := range pylint.MessageTypes {
		msgsPerType := msgs[pylint.MessageType(msgType)]
		formatted += fmt.Sprintf(color.YellowString("    %s: ", msgType)+"%d, %+v\n", sum(msgsPerType), msgsPerType)
	}
	return formatted
}

func sum(m map[string]int) int {
	total := 0
	for _, count := range m {
		total += count
	}
	return total
}

// ------------------------------------------------------------------

func (res Results) ToYAML() ([]byte, error) {
	return yaml.Marshal(res)
}

func (res ResultsFile) ToYAML() ([]byte, error) {
	return yaml.Marshal(res)
}

func (res ResultsFile) ToYAMLFile(filename string) error {
	resultsYml, err := res.ToYAML()
	if err != nil {
		return fmt.Errorf("failed to convert results to YAML: %w", err)
	}

	err = ioutil.WriteFile(filename, resultsYml, 0755)
	if err != nil {
		return fmt.Errorf("failed to save results to file: %w", err)
	}

	return nil
}

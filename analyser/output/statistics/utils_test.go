package statistics_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/output/statistics"
)

func TestAddCounts(t *testing.T) {
	counts := map[string]int{
		"msg0": 0,
		"msg1": 1,
		"msg2": 2,
		"msg3": 3,
		"msg4": 4,
		"msg5": 5,
	}

	add := map[string]int{
		"msg0": 5,
		"msg6": 1,
		"msg3": 3,
	}

	expected := map[string]int{
		"msg0": 5,
		"msg1": 1,
		"msg2": 2,
		"msg3": 6,
		"msg4": 4,
		"msg5": 5,
		"msg6": 1,
	}

	res := statistics.AddCounts(counts, add)
	require.Equal(t, expected, res)
	require.NotEqual(t, expected, counts)
	require.NotEqual(t, expected, add)
}

func TestSortMostFrequent(t *testing.T) {
	counts := map[string]int{
		"msg0": 5,
		"msg1": 1,
		"msg2": 2,
		"msg3": 6,
		"msg4": 4,
		"msg5": 5,
		"msg6": 1,
	}

	expected := statistics.PairList{
		statistics.Pair{Item: "msg3", Count: 6},
		statistics.Pair{Item: "msg0", Count: 5},
		statistics.Pair{Item: "msg5", Count: 5},
		statistics.Pair{Item: "msg4", Count: 4},
		statistics.Pair{Item: "msg2", Count: 2},
		statistics.Pair{Item: "msg1", Count: 1},
		statistics.Pair{Item: "msg6", Count: 1},
	}

	sorted := statistics.SortMostFrequent(counts)
	require.Equal(t, expected, sorted)
}

package ipynb_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/ipynb"
)

func TestCountLoC(t *testing.T) {
	filename := "test-resources/sample.ipynb"
	nb, err := ipynb.Open(filename)
	require.NoError(t, err)
	count := nb.CountLoC()
	require.Equal(t, 25, count)
}

func TestCountLoCInterviewFixed(t *testing.T) {
	filename := "test-resources/InterviewNewlineErrorFixed.ipynb"
	nb, err := ipynb.Open(filename)
	require.NoError(t, err)
	count := nb.CountLoC()
	require.Equal(t, 125, count)
}

func TestCountLoCInterview(t *testing.T) {
	filenameFixed := "test-resources/InterviewNewlineErrorFixed.ipynb"
	filename := "test-resources/InterviewNewlineError.ipynb"
	count, err := ipynb.CountTotalLoC([]string{filename, filenameFixed})
	require.Error(t, err)
	require.True(t, errors.Is(err, ipynb.ErrPartialCount))
	require.Equal(t, 125, count) // the fixed file will still be counted.
}

package utils_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/utils"
)

func TestCountLinesInFile(t *testing.T) {
	filename := "test-resources/test-count-lines"
	count, err := utils.CountLinesInFile(filename)
	require.NoError(t, err)
	require.Equal(t, 30, count)

	filename = "test-resources/test-count-lines2"
	count, err = utils.CountLinesInFile(filename)
	require.NoError(t, err)
	require.Equal(t, 17, count)
}

func TestCountLinesInFiles(t *testing.T) {
	filenames := []string{
		"test-resources/test-count-lines",
		"test-resources/test-count-lines2",
	}

	count, err := utils.CountLinesInFiles(filenames)
	require.NoError(t, err)
	require.Equal(t, 47, count)
}

func TestCountLinesInFilesEmpty(t *testing.T) {
	filenames := []string{}
	count, err := utils.CountLinesInFiles(filenames)
	require.NoError(t, err)
	require.Equal(t, 0, count)
}

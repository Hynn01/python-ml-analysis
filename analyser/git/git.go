package git

import (
	"fmt"
	"os/exec"
	"path"
	"strconv"
	"strings"

	"gitlab.com/bvobart/python-ml-analysis/utils"
)

// Clone calls `git clone` to clone the Git repo at the provided url into the
// specified target directory.
func Clone(url string, targetDir string) error {
	cmd := exec.Command("git", "clone", url, targetDir)
	if _, err := cmd.Output(); err != nil {
		return utils.WrapExitError(err)
	}
	return nil
}

// GetCurrentCommitHash returns the hash of the commit that the Git repository in the
// specified directory is currently pointing at.
func GetCurrentCommitHash(repoDir string) (string, error) {

	if !utils.DirExists(path.Join(repoDir, ".git")) {
		return "no git", nil
	}

	cmd := exec.Command("git", "rev-parse", "HEAD")
	cmd.Dir = repoDir
	output, err := cmd.Output()
	if err != nil {
		return "", utils.WrapExitError(err)
	}

	hash := strings.TrimSpace(string(output))
	return hash, nil
}

// CountCommits counts the number of commits made to the currently referenced branch
// in the Git repository in the specified directory
func CountCommits(repoDir string) (int, error) {

	if !utils.DirExists(path.Join(repoDir, ".git")) {
		return 0, nil
	}

	cmd := exec.Command("git", "rev-list", "--count", "HEAD")
	cmd.Dir = repoDir
	output, err := cmd.Output()
	if err != nil {
		return -1, utils.WrapExitError(err)
	}

	count, err := strconv.ParseInt(strings.TrimSpace(string(output)), 10, 32)
	if err != nil {
		return -1, fmt.Errorf("failed to parse commit count: %w", err)
	}

	return int(count), nil
}

// CountContributors counts the number of contributors to the default branch in the Git repository
// in the specified directory.
func CountContributors(repoDir string) (int, error) {

	if !utils.DirExists(path.Join(repoDir, ".git")) {
		return 0, nil
	}

	cmd := exec.Command("git", "shortlog", "-s", "-n", "HEAD")
	cmd.Dir = repoDir
	output, err := cmd.CombinedOutput()
	if err != nil {
		return -1, utils.WrapExitError(err)
	}

	count, err := utils.CountLinesInReader(strings.NewReader(string(output)))
	if err != nil {
		return -1, err
	}

	return count, nil
}

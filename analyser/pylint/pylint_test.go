package pylint_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/pylint"
	"gitlab.com/bvobart/python-ml-analysis/virtualenv"
	"gopkg.in/yaml.v3"
)

var exampleMessages = []pylint.Message{
	{Path: "file1.py", Line: 5, Column: 0, Type: "warning", Symbol: "bad-indentation", SymbolID: "W0311"},
	{Path: "file1.py", Line: 1, Column: 0, Type: "convention", Symbol: "missing-module-docstring", SymbolID: "C0114"},
	{Path: "file1.py", Line: 2, Column: 0, Type: "convention", Symbol: "invalid-name", SymbolID: "C0103"},
	{Path: "file1.py", Line: 4, Column: 0, Type: "convention", Symbol: "invalid-name", SymbolID: "C0103"},
	{Path: "file1.py", Line: 4, Column: 0, Type: "convention", Symbol: "missing-function-docstring", SymbolID: "C0116"},
	{Path: "file1.py", Line: 7, Column: 0, Type: "error", Symbol: "undefined-variable", SymbolID: "E0602"},
	{Path: "file1.py", Line: 1, Column: 0, Type: "warning", Symbol: "unused-import", SymbolID: "W0611"},
	{Path: "folder/file2.py", Line: 1, Column: 0, Type: "convention", Symbol: "missing-module-docstring", SymbolID: "C0114"},
	{Path: "folder/file2.py", Line: 1, Column: 31, Type: "error", Symbol: "undefined-variable", SymbolID: "E0602"},
}

func TestAnalyseAll(t *testing.T) {
	dir := "test-resources/test-project"
	if !virtualenv.Is(dir) {
		_, err := virtualenv.Create(dir)
		require.NoError(t, err)
	}

	messages, err := pylint.AnalyseAll(dir, dir)
	require.NoError(t, err)
	require.NotEmpty(t, messages)
	require.Equal(t, exampleMessages, messages)
}

func TestAnalyseAllNoFiles(t *testing.T) {
	dir := "../input/test-resources"
	messages, err := pylint.AnalyseAll(dir, dir)
	require.Error(t, err)
	require.True(t, errors.Is(err, pylint.ErrNoPythonFiles))
	require.Nil(t, messages)
}

func TestMessageToYAMLandBack(t *testing.T) {
	yml, err := yaml.Marshal(exampleMessages)
	require.NoError(t, err)

	transformed := []pylint.Message{}
	err = yaml.Unmarshal(yml, &transformed)
	require.NoError(t, err)

	require.Equal(t, exampleMessages, transformed)
}

func TestMessageToYAML(t *testing.T) {
	msg := pylint.Message{Path: "file1.py", Line: 5, Column: 0, Type: "warning", Symbol: "bad-indentation", SymbolID: "W0311"}
	yml, err := yaml.Marshal(msg)
	require.NoError(t, err)
	require.Equal(t, "file1.py:5:0:warning:bad-indentation:W0311\n", string(yml))
}

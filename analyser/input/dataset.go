package input

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/bvobart/python-ml-analysis/utils"
	"gopkg.in/yaml.v3"
)

// Dataset is the structure of the dataset.yml file.
type Dataset struct {
	Projects []Project `yaml:"projects"`
}

// Project is one entry in the dataset.
// See `yamlProject` below for a better view on what can be defined in the YAML file.
type Project struct {
	ID   int
	Name string // derived from CloneURL or Folder Name

	// URL to `git clone` the code's repo with
	CloneURL string `yaml:"url"`

	FolderName string `yaml:"folder"`

	// Folder under 'projects' into which the Git repo should be cloned
	CloneFolder string // derived from CloneURL or Folder Name

	// Defines the location of the requirements.txt file in the repo, if this is not at the root of the repo,
	// but is located somewhere else in the repo.
	RequirementsFile string `yaml:"requirements"`

	// Defines the contents of a custom requirements.txt file. Useful if the project does not contain
	// a requirements.txt file originally and you need more control over the requirements file than just
	// having it generated with pipreqs.
	RequirementsContents string `yaml:"requirements.txt"`

	// Any additional requirements that need to be installed one by one after installing regular requirements.
	ExtraRequirements []string `yaml:"extraRequirements"`

	// Defines the subfolder of the repo in which the project is located.
	// Useful for monorepos where there are lots of projects in one repo.
	SubFolder string

	// If true, this project and any other projects with Only == true will be the only projects exectuted,
	// all other projects will be ignored.
	Only bool `yaml:"only"`
}

func (ds Dataset) Validate() error {
	for i, project := range ds.Projects {
		if err := project.Validate(); err != nil {
			return fmt.Errorf("project %d - %s is invalid: %w", i, project.Name, err)
		}
	}
	return nil
}

func (p Project) Validate() error {
	if p.CloneURL == "" && p.FolderName == "" {
		return fmt.Errorf("no `url` and `folder` provided")
	}
	return nil
}

// ------------------------------------------------------------------

// ReadDataset reads and parses our dataset from the file at the given location.
func ReadDataset(filename string) (*Dataset, error) {
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	ds := yamlDataset{}
	yaml.Unmarshal(file, &ds)

	dataset := ds.Decode()
	return &dataset, dataset.Validate()
}

// ------------------------------------------------------------------

type yamlDataset struct {
	Projects []yamlProject `yaml:"projects"`
}

type yamlProject struct {
	// URL to `git clone` the code's repo with
	CloneURL string `yaml:"url"`

	// Folder for the project
	FolderName string `yaml:"folder"`

	// Defines the location of the requirements.txt file in the repo, if this is not at the root of the repo,
	// but is located somewhere else in the repo.
	RequirementsFile string `yaml:"requirements"`

	// Defines the contents of a custom requirements.txt file. Useful if the project does not contain
	// a requirements.txt file originally and you need more control over the requirements file than just
	// having it generated with pipreqs.
	RequirementsContents string `yaml:"requirements.txt"`

	// Any additional requirements that need to be installed one by one after installing regular requirements.
	ExtraRequirements []string `yaml:"extraRequirements"`

	// Defines a list of subprojects that are inside this repository.
	// Each of these subprojects decodes to its own Project.
	Projects []struct {
		Folder               string `yaml:"folder"`
		RequirementsFile     string `yaml:"requirements"`
		RequirementsContents string `yaml:"requirements.txt"`
		// Any additional requirements that need to be installed one by one after installing regular requirements.
		ExtraRequirements []string `yaml:"extraRequirements"`
		Only              bool     `yaml:"only"`
		Skip              bool     `yaml:"skip"`
	} `yaml:"projects"`

	// If true, this project and any other projects with Only == true will be the only projects exectuted,
	// all other projects will be ignored.
	Only bool `yaml:"only"`

	// If true, this project will not be analysed.
	Skip bool `yaml:"skip"`
}

func (ds yamlDataset) Decode() Dataset {
	projects := []Project{}
	id := 0
	for _, p := range ds.Projects {
		projects = append(projects, p.Decode(&id)...)
	}
	return Dataset{Projects: projects}
}

func (p yamlProject) Decode(id *int) []Project {
	name := p.GetName()
	if p.Skip {
		return []Project{}
	}

	// when the project does not define any subprojects
	if len(p.Projects) == 0 {
		(*id)++
		return []Project{{
			ID:                   *id,
			Name:                 name,
			CloneURL:             p.CloneURL,
			FolderName:           p.FolderName,
			CloneFolder:          name,
			RequirementsFile:     utils.IfNotEmpty(p.RequirementsFile, "requirements.txt"),
			RequirementsContents: p.RequirementsContents,
			ExtraRequirements:    p.ExtraRequirements,
			SubFolder:            "",
			Only:                 p.Only,
		}}
	}

	// when the project does define subprojects, make a Project out of each subproject.
	projects := []Project{}
	for _, subProject := range p.Projects {
		if subProject.Skip {
			continue
		}

		(*id)++
		project := Project{
			ID:                   *id,
			Name:                 subProject.Folder + " (" + name + ")",
			CloneURL:             p.CloneURL,
			FolderName:           p.FolderName,
			CloneFolder:          name, // do not include subProject folder in clone folder so no need to reclone
			RequirementsFile:     utils.IfNotEmpty(subProject.RequirementsFile, "requirements.txt"),
			RequirementsContents: subProject.RequirementsContents,
			ExtraRequirements:    subProject.ExtraRequirements,
			SubFolder:            subProject.Folder,
			Only:                 subProject.Only || p.Only, // allows 'only' to be configured on parent or subproject.
		}
		projects = append(projects, project)
	}

	return projects
}

// ------------------------------------------------------------------

func (p yamlProject) GetName() string {
	if p.CloneURL != "" {
		return strings.TrimSuffix(p.CloneURL[strings.LastIndex(p.CloneURL, "/")+1:], ".git")
	} else {
		return p.FolderName
	}

}

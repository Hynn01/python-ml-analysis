package main

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	"github.com/fatih/color"
	"gitlab.com/bvobart/python-ml-analysis/input"
	"gitlab.com/bvobart/python-ml-analysis/output"
	"gitlab.com/bvobart/python-ml-analysis/output/csv"
	"gitlab.com/bvobart/python-ml-analysis/virtualenv"
)

const dir = "projects"
const datasetYML = "./data/dataset.yml"

// Maximum amount of parallel workers. Equal to amount of threads available on the CPU.
var maxWorkers = runtime.NumCPU()

var startTime = time.Now()
var timeString = startTime.Format("2006-01-02.15:04.MST")
var resultsYML = "./results/results-" + timeString + ".yml"
var resultsCSV = "./results/results-" + timeString + ".csv"

func main() {
	if err := run(); err != nil {
		log.Fatalf(color.RedString("failed to run analysis: ")+"%+v", err)
	}
}

func run() error {
	// read the dataset
	dataset, err := input.ReadDataset(datasetYML)
	if err != nil {
		return err
	}

	log.Println(color.GreenString("--> Found %d projects in dataset", len(dataset.Projects)), "time:", time.Since(startTime))

	// create projects folder
	err = os.MkdirAll(dir, 0755)
	if err != nil {
		return err
	}

	// set up analysers for each project
	analysers := sync.Map{}
	projects := selectProjects(dataset.Projects)
	for _, project := range projects {
		analyser := NewAnalyser(project, startTime)
		analysers.Store(project.ID, &analyser)
	}

	if len(projects) < len(dataset.Projects) {
		log.Println(color.GreenString("--> only %d projects in dataset were selected to be analysed", len(projects)), "time:", time.Since(startTime))
	}

	log.Println(color.GreenString("--> Starting analysis of each selected project"), "time:", time.Since(startTime))
	var done uint32
	var errored uint32

	createVirtualEnv()

	parallelForEach(projects, func(project input.Project) error {
		a, _ := analysers.Load(project.ID)
		analyser := a.(*Analyser)
		var err error

		if project.CloneURL != "" {
			err = analyser.CloneProject()
			if err != nil {
				goto onError
			}
		}

		_, err = analyser.CheckRequirementsFile()
		if err != nil {
			goto onError
		}

		analyser.Log("Starting analysis...")
		err = analyser.AnalyseProject()
		if err != nil {
			goto onError
		}

		log.Printf(color.GreenString("--> Project %d - Done! Results: ", analyser.Project.ID)+"time: %s%s\n", time.Since(startTime), analyser.Results.String())
		atomic.AddUint32(&done, 1)
		printCompleted(&done, &errored, len(projects))
		return nil

	onError:
		analyser.Results.Error = err.Error()
		log.Println(err)
		atomic.AddUint32(&done, 1)
		atomic.AddUint32(&errored, 1)
		printCompleted(&done, &errored, len(projects))
		return err
	})

	log.Println(color.GreenString("--> Finished analysis of all projects! Collecting all results..."), "time:", time.Since(startTime))

	// Collect results from all analysers
	resultsFile := output.ResultsFile{}
	analysers.Range(func(key, value interface{}) bool {
		analyser := value.(*Analyser)
		resultsFile.Results = append(resultsFile.Results, analyser.Results)
		return true
	})

	// Save to YAML
	if err := resultsFile.ToYAMLFile(resultsYML); err != nil {
		return err
	}
	log.Println(color.GreenString("--> YAML results saved to %s", resultsYML), "time:", time.Since(startTime))

	// Save to CSV
	if err := csv.FromResultsFile(resultsFile).WriteFile(resultsCSV); err != nil {
		return err
	}
	log.Println(color.GreenString("--> CSV results saved to %s", resultsCSV), "time:", time.Since(startTime))

	log.Println(color.GreenString("Done!"), "time:", time.Since(startTime))
	return nil
}

func parallelForEach(projectlist []input.Project, f func(project input.Project) error) {
	projects := make(chan input.Project, maxWorkers)
	errors := make(chan error, 1)
	wg := sync.WaitGroup{}

	// send all projects through the channel
	go func() {
		for _, project := range projectlist {
			projects <- project
		}
		close(projects)
	}()

	// start workers to process projects
	for i := 0; i < maxWorkers; i++ {
		wg.Add(1)

		// until channel is closed, receive project, call f on it and report back the error if there was any
		go func() {
			defer wg.Done()
			for {
				project, open := <-projects
				if !open {
					return
				}

				err := f(project)
				if err != nil {
					errors <- err
				}
			}
		}()
	}

	// wait for workers to finish, then close errors channel so we can return from this function
	go func() {
		wg.Wait()
		close(errors)
	}()

	for {
		_, open := <-errors
		if !open {
			break // exit the loop when errors channel is closed.
		}
	}
}

func printCompleted(done *uint32, errored *uint32, total int) {
	template := color.GreenString("--> Completed: ") + "%d" + color.GreenString("/") + "%d" + color.GreenString(" projects")

	msg := ""
	if numErrors := atomic.LoadUint32(errored); numErrors > 0 {
		template += color.GreenString(", ") + "%d" + color.RedString(" had errors")
		msg = fmt.Sprintf(template, atomic.LoadUint32(done), total, numErrors)
	} else {
		msg = fmt.Sprintf(template, atomic.LoadUint32(done), total)
	}

	log.Println(msg, "time:", time.Since(startTime))
}

// Selects the projects that should be analysed.
// Returns either all projects marked with `only: true` or all projects if no projects were marked as such.
func selectProjects(projects []input.Project) []input.Project {
	onlyProjects := []input.Project{}
	for _, p := range projects {
		if p.Only {
			onlyProjects = append(onlyProjects, p)
		}
	}

	if len(onlyProjects) > 0 {
		return onlyProjects
	}
	return projects
}

// CreateVirtualEnv creates a virtualenv in the project folder
func createVirtualEnv() error {
	projectDir := "projects"
	if _, err := virtualenv.Source(projectDir); err == nil {
		log.Print("Already contains a virtualenv!")
		return nil
	}

	log.Print("Creating a virtualenv...")
	_, err := virtualenv.Create(projectDir)
	if err != nil {
		return fmt.Errorf("failed to create virtualenv: %w", err)
	}

	return nil
}
